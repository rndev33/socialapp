import React, {useEffect} from 'react';
import AppStack from './stacks/appStack';
import {useDispatch, useSelector} from 'react-redux';
import AuthStack from './stacks/authStack';


const Routes = () => {
  const dispatch = useDispatch();
  const {user} = useSelector((state: any) => state.root.user);
  const {onboarding} = useSelector((state: any) => state.root.genral);

  return user ? <AppStack /> :  <AuthStack />;
};

export default Routes;
