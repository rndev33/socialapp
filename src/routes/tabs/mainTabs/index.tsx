import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeStack from '@routes/stacks/homeStack';
import {RF} from '@theme/responsive';
import {SIZING} from '@theme/sizing';
import {IOS} from '@utils/constants';
import React from 'react';
import {StyleSheet} from 'react-native';


const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route: {name}}) => ({
        tabBarItemStyle: {justifyContent: 'center'},
        tabBarShowLabel: false,
        tabBarStyle: styles.tabBarStyle,
        headerShown: false,
        tabBarActiveTintColor: "gray",
        tabBarInactiveTintColor: "black",
        tabBarHideOnKeyboard: true,
        tabBarIconStyle: styles.tabBarIconStyle,
      })}>
      <Tab.Screen name="Home" component={HomeStack} />
      <Tab.Screen name="Homea" component={HomeStack} />
      <Tab.Screen name="Homeb" component={HomeStack} />
      <Tab.Screen name="Homec" component={HomeStack} />
    </Tab.Navigator>
  );
};

export default MyTabs;

const styles = StyleSheet.create({
  tabBarStyle: {
    ...SIZING.h10,
  },
  icon: {
    width: RF(20),
    height: RF(23.5),
  },
  tabBarIconStyle: {
    width: RF(20),
    maxHeight: RF(30),
    marginTop: IOS ? RF(10) : 0,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
});
