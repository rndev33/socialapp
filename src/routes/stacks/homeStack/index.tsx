import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '@screens/general/home';
import {ROUTES} from '@utils/routes';
import React from 'react';
import {StatusBar} from 'react-native';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <>
      <StatusBar
        barStyle="dark-content"
        translucent
        backgroundColor="transparent"
      />
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name={ROUTES.HOMESCREEN} component={HomeScreen} />
    
  
      </Stack.Navigator>
    </>
  );
};

export default HomeStack;
