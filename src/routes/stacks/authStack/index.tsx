import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StatusBar} from 'react-native';
import {ROUTES} from '@utils/routes';
import InitialAuthScreen from '@screens/auth/initaialScreen';


const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <>
      <StatusBar
        barStyle="dark-content"
        translucent
        backgroundColor="transparent"
      />

      <Stack.Navigator
        initialRouteName={ROUTES.INITIALAUTH}
        screenOptions={{headerShown: false}}>

        <Stack.Screen name={ROUTES.INITIALAUTH} component={InitialAuthScreen} />
     
      </Stack.Navigator>
    </>
  );
};

export default AuthStack;
