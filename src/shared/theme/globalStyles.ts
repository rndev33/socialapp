import {StyleSheet} from 'react-native';
import {COLORS} from './colors';
import {RF} from './responsive';
import {SIZING} from './sizing';
import {SPACING} from './spacing';

export const GST = StyleSheet.create({
  ...SPACING,
  ...SIZING,



});
