import {COLORS} from '@theme/colors';
import { RF } from '@theme/responsive';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE_BG,
  },
  PaddingBottom:{paddingBottom:RF(120)}
});
