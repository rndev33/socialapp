import { INITBG } from '@assets/images';
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';

const InitialAuthScreen = () => {
    return (
      <ImageBackground style={{flex:1}} source={INITBG}>

      </ImageBackground>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

export default InitialAuthScreen;
